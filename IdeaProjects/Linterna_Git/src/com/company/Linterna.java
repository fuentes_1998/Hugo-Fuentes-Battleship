package com.company;

public class Linterna {

    private String brand;

    private String model;

    private boolean status;

    private LanternMode mode;

    private Bateria battery;

    Linterna(String brand, String model, Bateria battery) {

        this.brand = brand;
        this.model = model;
        this.battery = battery;
        this.status = false;
    }
    public void setStatus(boolean status) {

        this.status = status;

        if (status) {

            if (this.battery.getPercentBattery() == 0) {

                System.out.println("Batería agotada");
                this.status = false;
            } else {

                unloadBattery();
            }
        }
    }
    public void setMode(LanternMode mode) {

        this.mode = mode;
    }
    private void unloadBattery() {

        this.battery.unloadPercentBattery();
    }
    public void loadBattery() {

        this.battery.loadPercentBattery();
    }
    public void showPercentBattery() {

        System.out.println(this.battery.getPercentBattery());
    }
}
