package com.company;

public class Bateria {

    private String brand;

    private String model;

    private int percentBattery;

    Bateria(String brand, String model) {

        this.brand = brand;
        this.model = model;
        this.percentBattery = 100;
    }
    public void unloadPercentBattery() {

        this.percentBattery = this.percentBattery - (int) (Math.random() * (this.percentBattery - 1 + 1) + 1);
    }
    public int getPercentBattery() {

        return this.percentBattery;
    }
    public void loadPercentBattery() {

        this.percentBattery = this.percentBattery + (int) (Math.random() * ((99 - this.percentBattery) - 1 + 1) + 1);
    }
}
