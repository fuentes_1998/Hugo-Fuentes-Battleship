package com.company;

public class Main {

    public static void main(String[] args) {

        Bateria battery1 = new Bateria("Xiaomi", "PowerBank");

        Linterna linterna1 = new Linterna("Ultrafire", "698LM", battery1);

        linterna1.setStatus(true);

        linterna1.setMode(LanternMode.FAST_BLINK);

        linterna1.loadBattery();

        linterna1.showPercentBattery();

        Bateria battery2 = new Bateria("LG", "GP23");

        Linterna linterna2 = new Linterna("Olfa", "XM23", battery2);

        linterna2.setStatus(true);

        linterna2.setMode(LanternMode.SLOW_BLINK);

        linterna2.setMode(LanternMode.FIXED_LIGHT);

        linterna2.setStatus(false);

        linterna2.loadBattery();

        linterna2.showPercentBattery();
    }
}
