package com.company;

public enum LanternMode {

    FAST_BLINK, FIXED_LIGHT, SLOW_BLINK
}
