package com.company;

public class Main {

    public static void main(String[] args) {

        Player player = new Player();

        Computer computer = new Computer();

        Game game = new Game(player, computer);

        game.startGame();
    }
}
