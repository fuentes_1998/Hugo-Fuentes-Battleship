package com.company;

public class Computer {

    private Board board;

    private Board shotBoard;

    Computer() {

        this.board = new Board();

        this.shotBoard = new Board();
    }

    public Board getBoard() {

        return this.board;
    }

    public Board getShotBoard() {

        return this.shotBoard;
    }
}
