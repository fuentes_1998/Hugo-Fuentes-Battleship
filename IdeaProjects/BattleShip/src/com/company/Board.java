package com.company;

import java.util.Random;

public class Board {

    private String[][] board;

    private final String[] boat1;

    private final String[] boat2;

    private final String[] boat3;

    private final String water;

    Board() {

        this.board = new String[10][10];

        this.boat1 = new String[4];

        this.boat1[0] = "@";
        this.boat1[1] = "@";
        this.boat1[2] = "@";
        this.boat1[3] = "@";

        this.boat2 = new String[2];

        this.boat2[0] = "€";
        this.boat2[1] = "€";

        this.boat3 = new String[3];

        this.boat3[0] = "#";
        this.boat3[1] = "#";
        this.boat3[2] = "#";

        this.water =  "~";
    }

    public void printBoard() {

        for (int i = 0; i < this.board.length; i++) {

            for (int j = 0; j < this.board[i].length; j++) {

                this.board[i][j] = this.water;
            }
        }
    }

    public void randomBoatDealer() {

        Random rnd = new Random();

        int rowPosition = rnd.nextInt(9+1);

        int colPosition = rnd.nextInt(9+1);

        int randomAux = rnd.nextInt(3+1);

        int aux;

        for (int i = 0; i < this.boat3.length; i++) {

            if (i == 1) {

                do {

                    randomAux = rnd.nextInt(3 + 1);

                } while (aroundNumbers(rowPosition, colPosition)[randomAux] == 10);

                if (randomAux % 2 == 0) {

                    aux = rowPosition;

                    rowPosition = aroundNumbers(aux, colPosition)[randomAux];

                } else {

                    aux = colPosition;

                    colPosition = aroundNumbers(rowPosition, aux)[randomAux];
                }

            }else if(i == 2) {

                if (randomAux == 0) {

                    if (rowPosition == 9) {

                        rowPosition = 7;
                    } else {

                        rowPosition++;
                    }
                } else if (randomAux == 1) {

                    if (colPosition == 9) {

                        colPosition = 7;
                    } else {

                        colPosition++;
                    }
                } else if (randomAux == 2) {

                    if (rowPosition == 0) {

                        rowPosition = 2;
                    } else {

                        rowPosition--;
                    }
                } else {

                    if (colPosition == 0) {

                        colPosition = 2;
                    } else {

                        colPosition--;
                    }
                }
            }

            this.board[rowPosition][colPosition] = this.boat3[i];

        }

        do {

            rowPosition = rnd.nextInt(9+1);

            colPosition = rnd.nextInt(9+1);

        }while ((this.board[rowPosition][colPosition].equals(this.boat3[0])
                || this.board[rowPosition][colPosition].equals(this.boat3[1])
                || this.board[rowPosition][colPosition].equals(this.boat3[2]))
                || checkAround3Boats(rowPosition, colPosition));

        this.board[rowPosition][colPosition] = this.boat2[0];

        int auxRow = rowPosition;

        int auxCol = colPosition;

        do {

            rowPosition = auxRow;

            colPosition = auxCol;

            randomAux = rnd.nextInt(1+1);

            if (randomAux == 0) {

                randomAux = rnd.nextInt(1+1);

                if (randomAux == 0) {

                    if (rowPosition != 9) {

                        rowPosition++;
                    } else {

                        rowPosition--;
                    }
                } else {

                    if (colPosition != 9) {

                        colPosition++;
                    } else {

                        colPosition--;
                    }
                }

            } else {

                randomAux = rnd.nextInt(1+1);

                if (randomAux == 0) {

                    if (rowPosition != 0) {

                        rowPosition--;
                    } else {

                        rowPosition++;
                    }
                } else {

                    if (colPosition != 0) {

                        colPosition--;
                    } else {

                        colPosition++;
                    }
                }

            }

        }while(((this.board[rowPosition][colPosition].equals(this.boat3[0])
                || this.board[rowPosition][colPosition].equals(this.boat3[1])
                || this.board[rowPosition][colPosition].equals(this.boat3[2]))
                || checkAround3Boats(rowPosition, colPosition))
                && (rowPosition != auxRow || colPosition != auxCol));

        this.board[rowPosition][colPosition] = this.boat2[1];

        for (int i = 0; i < this.boat1.length; i++) {

            do {

                rowPosition = rnd.nextInt(9+1);

                colPosition = rnd.nextInt(9+1);

            }while (((this.board[rowPosition][colPosition].equals(this.boat3[0])
                    || this.board[rowPosition][colPosition].equals(this.boat3[1])
                    || this.board[rowPosition][colPosition].equals(this.boat3[2])
                    || this.board[rowPosition][colPosition].equals(this.boat2[0])
                    || this.board[rowPosition][colPosition].equals(this.boat2[1])
                    || this.board[rowPosition][colPosition].equals(this.boat1[0])
                    || this.board[rowPosition][colPosition].equals(this.boat1[1])
                    || this.board[rowPosition][colPosition].equals(this.boat1[2])
                    || this.board[rowPosition][colPosition].equals(this.boat1[3]))
                    || checkAroundBoats(rowPosition, colPosition)));

            this.board[rowPosition][colPosition] = this.boat1[i];
        }
    }

    public void enemyShot(int[] shot) {

        if (this.board[shot[0]][shot[1]].equals(this.boat1[0])) {

            this.boat1[0] = null;

            this.board[shot[0]][shot[1]] = "O";

            System.out.println("Tocado y hundido.");
        } else if (this.board[shot[0]][shot[1]].equals(this.boat1[1])) {

            this.boat1[1] = null;

            this.board[shot[0]][shot[1]] = "O";

            System.out.println("Tocado y hundido.");
        } else if (this.board[shot[0]][shot[1]].equals(this.boat1[2])) {

            this.boat1[2] = null;

            this.board[shot[0]][shot[1]] = "O";

            System.out.println("Tocado y hundido.");
        } else if (this.board[shot[0]][shot[1]].equals(this.boat1[3])) {

            this.boat1[3] = null;

            this.board[shot[0]][shot[1]] = "O";

            System.out.println("Tocado y hundido.");
        } else if (this.board[shot[0]][shot[1]].equals(this.boat2[0])) {

            this.boat2[0] = null;

            this.board[shot[0]][shot[1]] = "O";

            if (this.boat2[1] == null) {

                System.out.println("Tocado y hundido.");
            } else {

                System.out.println("Tocado.");
            }
        } else if (this.board[shot[0]][shot[1]].equals(this.boat2[1])) {

            this.boat2[1] = null;

            this.board[shot[0]][shot[1]] = "O";

            if (this.boat2[0] == null) {

                System.out.println("Tocado y hundido.");
            } else {

                System.out.println("Tocado.");
            }
        } else if (this.board[shot[0]][shot[1]].equals(this.boat3[0])) {

            this.boat3[0] = null;

            this.board[shot[0]][shot[1]] = "O";

            if (this.boat3[1] == null && this.boat3[2] == null) {

                System.out.println("Tocado y hundido.");
            } else {

                System.out.println("Tocado.");
            }
        } else if (this.board[shot[0]][shot[1]].equals(this.boat3[1])) {

            this.boat3[1] = null;

            this.board[shot[0]][shot[1]] = "O";

            if (this.boat3[0] == null && this.boat3[2] == null) {

                System.out.println("Tocado y hundido.");
            } else {

                System.out.println("Tocado.");
            }
        } else if (this.board[shot[0]][shot[1]].equals(this.boat3[2])) {

            this.boat3[2] = null;

            this.board[shot[0]][shot[1]] = "O";

            if (this.boat3[0] == null && this.boat3[1] == null) {

                System.out.println("Tocado y hundido.");
            } else {

                System.out.println("Tocado.");
            }
        } else {

            this.board[shot[0]][shot[1]] = "+";

            System.out.println("Agua.");
        }

        System.out.println();
    }

    public boolean checkBoats() {

        if (this.boat1[0] == null
                && this.boat1[1] == null
                && this.boat1[2] == null
                && this.boat1[3] == null
                && this.boat2[0] == null
                && this.boat2[1] == null
                && this.boat3[0] == null
                && this.boat3[1] == null
                && this.boat3[2] == null) {

            return true;
        }

        return false;
    }

    public void showBoard() {

        System.out.println(" \tA B C D E F G H I J\n");

        for (int i= 0; i < this.board.length; i++) {

            for (int j = 0; j < this.board[i].length; j++) {

                if (j == 0) {

                    System.out.print((i+1) + "\t");
                }

                System.out.print(this.board[i][j] + " ");
            }

            System.out.println();
        }
    }

    public boolean touched(int[] shot) {

        if (this.board[shot[0]][shot[1]].equals(this.boat1[0])
                || this.board[shot[0]][shot[1]].equals(this.boat1[1])
                || this.board[shot[0]][shot[1]].equals(this.boat1[2])
                || this.board[shot[0]][shot[1]].equals(this.boat1[3])
                || this.board[shot[0]][shot[1]].equals(this.boat2[0])
                || this.board[shot[0]][shot[1]].equals(this.boat2[1])
                || this.board[shot[0]][shot[1]].equals(this.boat3[0])
                || this.board[shot[0]][shot[1]].equals(this.boat3[1])
                || this.board[shot[0]][shot[1]].equals(this.boat3[2])) {

            return true;
        }

        return false;
    }

    public void shotBoard(int[] shot, boolean touched) {

        if (touched) {

            this.board[shot[0]][shot[1]] = "O";
        } else {

            this.board[shot[0]][shot[1]] = "+";
        }
    }

    private boolean checkAround3Boats(int rowPosition, int colPosition) {

        if (rowPosition != 9 && colPosition != 9) {

            if (this.board[rowPosition + 1][colPosition + 1].equals(this.boat3[0])
                    || this.board[rowPosition + 1][colPosition + 1].equals(this.boat3[1])
                    || this.board[rowPosition + 1][colPosition + 1].equals(this.boat3[2])) {

                return true;
            }
        }

        if (rowPosition != 9 && colPosition != 0) {

            if (this.board[rowPosition + 1][colPosition - 1].equals(this.boat3[0])
                    || this.board[rowPosition + 1][colPosition - 1].equals(this.boat3[1])
                    || this.board[rowPosition + 1][colPosition - 1].equals(this.boat3[2])) {

                return true;
            }
        }

        if (rowPosition != 0 && colPosition != 0) {

            if (this.board[rowPosition - 1][colPosition - 1].equals(this.boat3[0])
                    || this.board[rowPosition - 1][colPosition - 1].equals(this.boat3[1])
                    || this.board[rowPosition - 1][colPosition - 1].equals(this.boat3[2])) {

                return true;
            }
        }

        if (rowPosition != 0 && colPosition != 9) {

            if (this.board[rowPosition - 1][colPosition + 1].equals(this.boat3[0])
                    || this.board[rowPosition - 1][colPosition + 1].equals(this.boat3[1])
                    || this.board[rowPosition - 1][colPosition + 1].equals(this.boat3[2])) {

                return true;
            }
        }

        for (int i = 0; i < aroundNumbers(rowPosition, colPosition).length; i++) {

            if (aroundNumbers(rowPosition, colPosition)[i] != 10) {

                if (i % 2 == 0) {

                    if (this.board[aroundNumbers(rowPosition, colPosition)[i]][colPosition].equals(this.boat3[0])
                            || this.board[aroundNumbers(rowPosition, colPosition)[i]][colPosition].equals(this.boat3[1])
                            || this.board[aroundNumbers(rowPosition, colPosition)[i]][colPosition].equals(this.boat3[2])) {

                        return true;
                    }
                } else {

                    if (this.board[rowPosition][aroundNumbers(rowPosition, colPosition)[i]].equals(this.boat3[0])
                            || this.board[rowPosition][aroundNumbers(rowPosition, colPosition)[i]].equals(this.boat3[1])
                            || this.board[rowPosition][aroundNumbers(rowPosition, colPosition)[i]].equals(this.boat3[2])) {

                        return true;
                    }
                }
            }
        }

        return false;
    }

    private boolean checkAroundBoats(int rowPosition, int colPosition) {

        if (rowPosition != 9 && colPosition != 9) {

            if (this.board[rowPosition + 1][colPosition + 1].equals(this.boat3[0])
                    || this.board[rowPosition + 1][colPosition + 1].equals(this.boat3[1])
                    || this.board[rowPosition + 1][colPosition + 1].equals(this.boat3[2])
                    || this.board[rowPosition + 1][colPosition + 1].equals(this.boat2[0])
                    || this.board[rowPosition + 1][colPosition + 1].equals(this.boat2[1])
                    || this.board[rowPosition + 1][colPosition + 1].equals(this.boat1[0])
                    || this.board[rowPosition + 1][colPosition + 1].equals(this.boat1[1])
                    || this.board[rowPosition + 1][colPosition + 1].equals(this.boat1[2])
                    || this.board[rowPosition + 1][colPosition + 1].equals(this.boat1[3])) {

                return true;
            }
        }

        if (rowPosition != 9 && colPosition != 0) {

            if (this.board[rowPosition + 1][colPosition - 1].equals(this.boat3[0])
                    || this.board[rowPosition + 1][colPosition - 1].equals(this.boat3[1])
                    || this.board[rowPosition + 1][colPosition - 1].equals(this.boat3[2])
                    || this.board[rowPosition + 1][colPosition - 1].equals(this.boat2[0])
                    || this.board[rowPosition + 1][colPosition - 1].equals(this.boat2[1])
                    || this.board[rowPosition + 1][colPosition - 1].equals(this.boat1[0])
                    || this.board[rowPosition + 1][colPosition - 1].equals(this.boat1[1])
                    || this.board[rowPosition + 1][colPosition - 1].equals(this.boat1[2])
                    || this.board[rowPosition + 1][colPosition - 1].equals(this.boat1[3])) {

                return true;
            }
        }

        if (rowPosition != 0 && colPosition != 0) {

            if (this.board[rowPosition - 1][colPosition - 1].equals(this.boat3[0])
                    || this.board[rowPosition - 1][colPosition - 1].equals(this.boat3[1])
                    || this.board[rowPosition - 1][colPosition - 1].equals(this.boat3[2])
                    || this.board[rowPosition - 1][colPosition - 1].equals(this.boat2[0])
                    || this.board[rowPosition - 1][colPosition - 1].equals(this.boat2[1])
                    || this.board[rowPosition - 1][colPosition - 1].equals(this.boat1[0])
                    || this.board[rowPosition - 1][colPosition - 1].equals(this.boat1[1])
                    || this.board[rowPosition - 1][colPosition - 1].equals(this.boat1[2])
                    || this.board[rowPosition - 1][colPosition - 1].equals(this.boat1[3])) {

                return true;
            }
        }

        if (rowPosition != 0 && colPosition != 9) {

            if (this.board[rowPosition - 1][colPosition + 1].equals(this.boat3[0])
                    || this.board[rowPosition - 1][colPosition + 1].equals(this.boat3[1])
                    || this.board[rowPosition - 1][colPosition + 1].equals(this.boat3[2])
                    || this.board[rowPosition - 1][colPosition + 1].equals(this.boat2[0])
                    || this.board[rowPosition - 1][colPosition + 1].equals(this.boat2[1])
                    || this.board[rowPosition - 1][colPosition + 1].equals(this.boat1[0])
                    || this.board[rowPosition - 1][colPosition + 1].equals(this.boat1[1])
                    || this.board[rowPosition - 1][colPosition + 1].equals(this.boat1[2])
                    || this.board[rowPosition - 1][colPosition + 1].equals(this.boat1[3])) {

                return true;
            }
        }

        for (int i = 0; i < aroundNumbers(rowPosition, colPosition).length; i++) {

            if (aroundNumbers(rowPosition, colPosition)[i] != 10) {

                if (i % 2 == 0) {

                    if (this.board[aroundNumbers(rowPosition, colPosition)[i]][colPosition].equals(this.boat3[0])
                            || this.board[aroundNumbers(rowPosition, colPosition)[i]][colPosition].equals(this.boat3[1])
                            || this.board[aroundNumbers(rowPosition, colPosition)[i]][colPosition].equals(this.boat3[2])
                            || this.board[aroundNumbers(rowPosition, colPosition)[i]][colPosition].equals(this.boat2[0])
                            || this.board[aroundNumbers(rowPosition, colPosition)[i]][colPosition].equals(this.boat2[1])
                            || this.board[aroundNumbers(rowPosition, colPosition)[i]][colPosition].equals(this.boat1[0])
                            || this.board[aroundNumbers(rowPosition, colPosition)[i]][colPosition].equals(this.boat1[1])
                            || this.board[aroundNumbers(rowPosition, colPosition)[i]][colPosition].equals(this.boat1[2])
                            || this.board[aroundNumbers(rowPosition, colPosition)[i]][colPosition].equals(this.boat1[3])) {

                        return true;
                    }
                } else {

                    if (this.board[rowPosition][aroundNumbers(rowPosition, colPosition)[i]].equals(this.boat3[0])
                            || this.board[rowPosition][aroundNumbers(rowPosition, colPosition)[i]].equals(this.boat3[1])
                            || this.board[rowPosition][aroundNumbers(rowPosition, colPosition)[i]].equals(this.boat3[2])
                            || this.board[rowPosition][aroundNumbers(rowPosition, colPosition)[i]].equals(this.boat2[0])
                            || this.board[rowPosition][aroundNumbers(rowPosition, colPosition)[i]].equals(this.boat2[1])
                            || this.board[rowPosition][aroundNumbers(rowPosition, colPosition)[i]].equals(this.boat1[0])
                            || this.board[rowPosition][aroundNumbers(rowPosition, colPosition)[i]].equals(this.boat1[1])
                            || this.board[rowPosition][aroundNumbers(rowPosition, colPosition)[i]].equals(this.boat1[2])
                            || this.board[rowPosition][aroundNumbers(rowPosition, colPosition)[i]].equals(this.boat1[3])) {

                        return true;
                    }
                }
            }
        }

        return false;
    }

    private int[] aroundNumbers(int rowPosition, int colPosition) {

        int[] aroundNumbers = new int[4];

        switch (rowPosition) {

            case 0:
            case 9:
                if (colPosition == 0 || colPosition == 9) {

                    if (rowPosition == 0 && colPosition == 0) {

                        aroundNumbers[0] = rowPosition + 1;
                        aroundNumbers[1] = colPosition + 1;
                        aroundNumbers[2] = 10;
                        aroundNumbers[3] = 10;
                    } else if (rowPosition == 0 && colPosition == 9) {

                        aroundNumbers[0] = rowPosition + 1;
                        aroundNumbers[1] = 10;
                        aroundNumbers[2] = 10;
                        aroundNumbers[3] = colPosition - 1;
                    } else if (rowPosition == 9 && colPosition == 0) {

                        aroundNumbers[0] = 10;
                        aroundNumbers[1] = colPosition + 1;
                        aroundNumbers[2] = rowPosition - 1;
                        aroundNumbers[3] = 10;
                    } else {

                        aroundNumbers[0] = 10;
                        aroundNumbers[1] = 10;
                        aroundNumbers[2] = rowPosition - 1;
                        aroundNumbers[3] = colPosition - 1;
                    }
                } else {

                    if (rowPosition == 0) {

                        aroundNumbers[0] = rowPosition+1;
                        aroundNumbers[1] = colPosition+1;
                        aroundNumbers[2] = 10;
                        aroundNumbers[3] = colPosition-1;
                    } else {

                        aroundNumbers[0] = 10;
                        aroundNumbers[1] = colPosition+1;
                        aroundNumbers[2] = rowPosition - 1;
                        aroundNumbers[3] = colPosition-1;
                    }
                }
                break;

            default:
                if (colPosition == 0 || colPosition == 9) {

                    if (colPosition == 0) {

                        aroundNumbers[0] = rowPosition+1;
                        aroundNumbers[1] = colPosition+1;
                        aroundNumbers[2] = rowPosition-1;
                        aroundNumbers[3] = 10;
                    } else {

                        aroundNumbers[0] = rowPosition+1;
                        aroundNumbers[1] = 10;
                        aroundNumbers[2] = rowPosition-1;
                        aroundNumbers[3] = colPosition-1;
                    }
                } else {

                    aroundNumbers[0] = rowPosition+1;
                    aroundNumbers[1] = colPosition+1;
                    aroundNumbers[2] = rowPosition-1;
                    aroundNumbers[3] = colPosition-1;
                }
                break;
        }

        return aroundNumbers;
    }
}
